﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(g151210095.Startup))]
namespace g151210095
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
